//
//  Controller.m
//  exampleToast
//
//  Created by raksha on 9/3/2021.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"


@interface RCT_EXTERN_MODULE(Controller, NSObject)

RCT_EXTERN_METHOD(requestAuthorization: (NSDictionary*)auth)

RCT_EXTERN_METHOD(sendTime: (NSDate*)time)

RCT_EXTERN_METHOD(getHealthData: (NSString*)time
                                type: (NSString*)type
                                data: (NSDictionary*)data
                                resolver: (RCTPromiseResolveBlock)resolve
                                rejecter: (RCTPromiseRejectBlock)reject)

@end
