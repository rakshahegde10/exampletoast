//
//  Controller.swift
//  exampleToast
//
//  Created by raksha on 9/3/2021.
//

import Foundation
import HealthKit

extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr);
        }
    }
    
    mutating func appendIfDictionary(key:String,json:JSON){
        if var dict = self.dictionary{
            dict[key] = json;
            self = JSON(dict);
        }
    }
}

extension Calendar {

    func endOfDay(for date: Date) -> Date {

        let dayStart = self.startOfDay(for: date)
        guard let nextDayStart = self.date(byAdding: .day, value: 1, to: dayStart) else {
            preconditionFailure("Expected start of next day")
        }
        var components = DateComponents()
        components.second = -1
        guard let dayEnd = self.date(byAdding: components, to: nextDayStart) else {
            preconditionFailure("Expected end of day")
        }
        return dayEnd
    }

}



@objc(Controller)
class Controller: NSObject {
  // Declares HealthKit store for our Project
  let healthStore = HKHealthStore()
      
      enum HealthDataType: String, Codable {
            case heartRate
            case bmi
            case bloodpressure
            case biologicalSex
            case stepCount
            case bloodGlucose
            case bodyTemperature
            case none
        }
       

        struct HealthDataItem: Codable {
            let endDate: String
            let value: String
            let startDate: String
            let uuid: String
            let type: String
        }
  
        class Data: NSObject {
          let endDate: String = ""
          let type: String = ""
          let startDate: String = ""
        }
        
        
        var myJSON: JSON = [
            "myDictionary": [String:AnyObject]()
        ]
        
        var myArray: [String] = [ ]
        
        var startDate: Date = Date()
        var endDate: Date = Date()
        
        var healthType: String = ""


  

  // Function Method to request Authorizations from Healthkit
     @objc
      func requestAuthorization(_ auth: NSDictionary) {
        
        var writeDataTypes: Set<HKObjectType> = []
        var readDataTypes: Set<HKObjectType> = []
        var readPerms: Set<HKObjectType> = []
        var writePerms: Set<HKObjectType>  = []
        var readPermsArray = NSArray()
        var writePermsArray = NSArray()

        readPermsArray = auth["read"] as! NSArray
        writePermsArray = auth["write"] as! NSArray
        print("read", readPermsArray)
        readPerms = getPermsFromOptions(readPermsArray);
        writePerms = getPermsFromOptions(writePermsArray);
 
            readDataTypes = readPerms;
            writeDataTypes = writePerms;
             print("readDataTypes", readDataTypes)
           
              
              healthStore.requestAuthorization(toShare: writeDataTypes as? Set<HKSampleType>, read: readDataTypes) { (success, error) in
                            print("Asked:",success)
                            //print("error:",error!)
                          
                            if (self.healthStore.authorizationStatus(for: HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)!) == .sharingAuthorized) {
                                print("Permission Granted to Access bloodGlucose")
                            } else {
                                print("Permission Denied to Access bloodGlucose")
                            }
                          
                            if !success {
                                // Handle the error here.
                            }
                        }
    }





                 func getPermsFromOptions(_ options: NSArray) -> Set<HKObjectType> {
                  var readPermSet: Set<HKObjectType> = []
                  var optionKey: String
                  var val: HKObjectType
                
                for i in options{
                  optionKey = i as! String
                  val = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier(rawValue: optionKey))!
                  readPermSet.insert(val)
                  print("set", readPermSet)
                        }
                      return readPermSet;
                    }

  
  
  
                @objc
                func sendTime(_ time: String) -> Void {
                                print("Time:", time);
                                let pre = String(time.prefix(16))
                                let suf = String(time.suffix(16))
                            //    let dateFormatter = DateFormatter()
                            //    dateFormatter.dateFormat = "EEE MMM dd yyyy hh:mm:ss"
                            //    let fromDate = dateFormatter.date(from: pre)
                            //    startDate = fromDate!
                            //    let toDate = dateFormatter.date(from: suf)
                            //    endDate = toDate!
                                print(suf, pre)
                                let fromDate = convertStringintoDate(dateStr: pre, dateFormat: "EEE MMM dd yyyy")
                                startDate = fromDate
                                let toDate = convertStringintoDate(dateStr: suf, dateFormat: "EEE MMM dd yyyy")
                                endDate = toDate
                                print(startDate, endDate)
                                _ = HealthDataItem(endDate: "", value: "", startDate: "", uuid: "", type: "")
                                self.myJSON["myDictionary"].appendIfDictionary(key:"HD", json: JSON([]))
                              }


                
                func convertStringintoDate(dateStr:String, dateFormat format:String) -> Date{
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale.init(identifier:"en_US_POSIX")
                    dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT")
                    dateFormatter.dateFormat = format
                    if let startDate = dateFormatter.date(from: dateStr){
                        return startDate as Date
                    }
                    return Date() as Date
                }

  
  
            @objc(getHealthData:type:data:resolver:rejecter:)
            func getHealthData( _ time: String, type: String, data: NSDictionary, resolver resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock){
            sendTime(time)
            print("object:", data)
            let object = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier(rawValue: type))
            let startOfDate = Calendar.current.startOfDay(for: startDate)
            let endOfDate = Calendar.current.endOfDay(for: endDate)
            print("date:",startOfDate, endOfDate)
            let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
            let query = HKSampleQuery(sampleType: object!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
                        for result in results as! [HKQuantitySample] {
                          let quan = result.quantity.description
                          let uuid = result.uuid.description
                          let dateFormatter = DateFormatter()
                          dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
                          let strStart = dateFormatter.string(from: result.startDate)
                          let strEnd = dateFormatter.string(from: result.endDate)
                            let heartRateItem = HealthDataItem(endDate: strEnd, value: String(quan), startDate: strStart, uuid:String(uuid), type: type)
                          do {
                                    let data = try JSONEncoder().encode(heartRateItem)
                                    let json = String(data: data, encoding: String.Encoding.utf8)
                                        if(self.myJSON["myDictionary"]["HD"].exists()){
                                          self.myJSON["myDictionary"]["HD"].appendIfArray(json: JSON(json as Any))
                                        } else {
                                          self.myJSON["myDictionary"].appendIfDictionary(key:"HD", json: JSON([json]))
                                        }
                              } catch {
                                //error handling
                              }
                        }
                            print("json xcode:", self.myJSON)
                           
              do {
                let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["HD"])
                let convertedString = String(data: data, encoding: String.Encoding.utf8)
                resolve(convertedString)
              } catch {
                
              }
                    }
                    healthStore.execute(query)

              }
  
  
                  

  
        @objc
         static func requiresMainQueueSetup() -> Bool {
         return true
        }
  
  
}
