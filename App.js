import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, SafeAreaView, ScrollView, Platform, TouchableOpacity, Alert, Modal, Pressable } from 'react-native';
import { Button, CheckBox } from 'react-native-elements';
import DatePicker from 'react-native-date-picker'
import { NativeModules, NativeEventEmitter } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { postFhirData } from './fhir.js';
import jsonFile from './bundle.json';
import ListItems from './ListItem.js';
import HealthkitController from './HealthKit.js';

const isIOS = Platform.OS === 'ios';
const isAndroid = Platform.OS === 'android';
let currentdate = new Date
var datetime = currentdate + "/" + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
datetime = datetime.substring(0, 16)


export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      name: "XYZ",
      title: "Health Data:",
      auth: false,
      from: "",
      to: "",
      picker: false,
      picker1: false,
      counter: 0,
      sex: "nil",
      age: "nil",
      bloodType: "nil",
      bg: "BG Values",
      hr: "HR Values",
      bt: "BT Values",
      bp: "BP Values",
      bmi: "BMI Values",
      nil: "nil",
      heartRate: [],
      bmi: [],
      bloodGlucose: [],
      bloodPressure: [],
      bodyTemp: [],
      drop: false,
      date: new Date(),
      date1: new Date(),
      sDate: datetime,
      tDate: datetime,
      pick: false,
      sendFromDate: "",
      full: false,
      type: "",
      avg: "",
      avgPlace: "GetAverage",
      hd: {},
      modal: false,
      hrReadCheck: false,
      hrWriteCheck: false,
      bmiReadCheck: false,
      bmiWriteCheck: false
    }
    Icon.loadFont()
   }

  clickHandlerBp = async() => {
    let OS = Platform.OS
    if(OS === "ios") {
    const log = await HealthkitController.getBloodPressure()
    .then(result => {
      let res = JSON.parse(result)
      const values = res.map(result => {
        let resy = JSON.parse(result);
        let fin = resy[0] 
        let val = fin.value
        let date = fin.startDate
        let final = val + "     -      " + date
        return final;
       });
       const data = Array.from(new Set(values)); 
      this.setState({bloodPressure : data}) 
      })
      } else {
        if(this.state.hd["BloodPressure"] !== undefined){
          let systolic = this.state.hd?.BloodPressure?.Systolic
          let diastolic = this.state.hd?.BloodPressure?.Diastolic
          let date = this.state.hd?.BloodPressure?.startDate
          let BP = systolic.substring(0,3) + "/" + diastolic.substring(0,2) + "  mm/Hg" + " - " + date
          let arr = new Array();
          arr.push(BP)
          const values = arr.map(result => {
            return result;
           });
          const data = Array.from(new Set(values)); 
          this.setState({bloodPressure: data}) 
        } else {
          // handle
        }
      }
  }

  // clickHandlerStep = async() => {
  //   const log = await HealthkitController.getSteps()
  //   .then(result => {
  //     let res = JSON.parse(result)
  //     for (let i = 0; i < res.length; i++) {
  //       let final = res[i];
  //       final = parseInt(final.value, 10)
  //       final = final.toString()
  //       this.setState({step : final}) 
  //   }});
  // }

  clickHandlerHearRate = async() => {
    let OS = Platform.OS
    if(OS === "ios") {
      let dat = this.state.sDate
      let final = dat.substring(2, 16)
      this.setState({sendFromDate: final})
      let dates = this.state.sDate + "," + this.state.tDate
      let type = 'HKQuantityTypeIdentifierHeartRate'
      let obj = {
        "startDate": new Date().toISOString(),
        "endDate": new Date().toISOString(),
        "type": "Hello"
      }
    const log = await HealthkitController.getHealthData(dates, type, obj)
    .then(result => {
    let res = JSON.parse(result)
    console.log("res:", res)
    const values = res.map(result => {
      const { value, startDate, type } = JSON.parse(result);
      if(type === 'HKQuantityTypeIdentifierHeartRate') {
      let final = value + "  -  " + startDate
      return final;
      }
     });
     const data = Array.from(new Set(values)); 
    this.setState({heartRate : data}) 
    })
  } else {
    if(this.state.hd["HeartRate"] !== undefined){
    let arr = new Array();
    let str = this.state.hd?.HeartRate?.HeartRate
    let date = this.state.hd?.HeartRate?.startDate
    let final = str + "  BPM" +  "   -   " + date
    arr.push(final)
    const values = arr.map(result => {
      return result;
     });
     const data = Array.from(new Set(values)); 
      this.setState({heartRate: data}) 
    } else {
      //handle
    }
  }
}

  clickHandlerBMI = async() => {
    let dat = this.state.sDate
      let final = dat.substring(2, 16)
      this.setState({sendFromDate: final})
      let dates = this.state.sDate + "," + this.state.tDate
    let type = 'HKQuantityTypeIdentifierBodyMassIndex'
    const log = await HealthkitController.getHealthData(dates, type)
    .then(result => {
      let res = JSON.parse(result)
      const values = res.map(result => {
        const { value, startDate, type } = JSON.parse(result);
        if(type === 'HKQuantityTypeIdentifierBodyMassIndex') {
        let final = value + "    -   " + startDate
        return final;
        }
       });
       const data = Array.from(new Set(values)); 
      this.setState({bmi : data}) 
      })
  }

  getBMIData = async() => {
    let OS = Platform.OS
    if(OS === "ios") {

    } else {
    NativeModules.MyHealth.getBodyHealthData((err ,result) => {
      let res = JSON.parse(result)      
      this.setState({hd: {...res}, full: true})
      console.log("hd again", this.state.hd)
  });

  if(this.state.hd["Weight"] !== undefined && this.state.hd["Height"] !== undefined){
    let arr = new Array();
    let weight = this.state.hd?.Weight?.Weight
    let height = this.state.hd?.Height?.Height
    let bodymassindex = weight / (height*height)
    let date = this.state.hd?.Weight?.startDate
    let final = bodymassindex.toFixed(2) + "  -  " + date
    arr.push(final)
    const values = arr.map(result => {
      return result;
     });
     const data = Array.from(new Set(values)); 
      this.setState({bmi: data}) 
    } else {
      //handle
    }
  }
  }

  clickHandlerBloodGlucose= async() =>   {
    const log = await HealthkitController.getBloodGlucose()
    .then(result => {
      let res = JSON.parse(result)
        const values = res.map(result => {
          const { value, startDate } = JSON.parse(result);
          let final = value + "     -      " + startDate
          return final;
         });
         const data = Array.from(new Set(values)); 
        this.setState({bloodGlucose : data})
  });
  }

  clickHandlerBodyTemperature= async() =>  {
    const log = await HealthkitController.getBodyTemperature()
    .then(result => {
      let res = JSON.parse(result)
      const values = res.map(result => {
        const { value, startDate } = JSON.parse(result);
        let final = value + "     -      " + startDate
        return final;
       });
       const data = Array.from(new Set(values)); 
      this.setState({bodyTemp : data}) 
      })
    // .then(result => {
    //   let res = JSON.parse(result)
    //   for (let i = 0; i < res.length; i++) {
    //     let final = res[i];
    //     final = final.value.toString()
    //     let finalVal = final.includes("degC")  ? final.substring(0, final.indexOf("degC")) : final.substring(0, final.indexOf("degF"))
    //     finalVal = finalVal.replaceAll(" ", "")
    //     jsonFile.entry[3].resource.valueQuantity.value = finalVal
    //     this.setState({bodyTemp : final}) 
    // }});
  }

  clickHandlerBloodType() {
    const log = HealthkitController.getBloodType()
    .then(result => {
        this.setState({bloodType : result}) 
    });
  }

  clickHandlerAge() {
    const log = HealthkitController.getAge()
    .then(result => {
      result = result.toString()
        this.setState({age : result}) 
    });
  }


  // clickHandlerSex = async() => {
  //   const log = await HealthkitController.sampleMethod()
  //   .then(result => {
  //     // let res = result.toString()
  //     console.warn("Sex:", result)
  //     // setSex(res)
  //   });

  // }

  requestAuth = async() => {
    let OS = Platform.OS
    if(OS === 'ios') {
      let obj = {
      permissions: {
        read: [
          "HKQuantityTypeIdentifierHeartRate",
          "HKQuantityTypeIdentifierBodyMassIndex"
        ],
        write: [
          "HKQuantityTypeIdentifierHeartRate",
          "HKQuantityTypeIdentifierBodyMassIndex"
        ]
       }
      }
    HealthkitController.requestAuthorization(obj)
    } else {
    NativeModules.MyHealth.getAuthorizations()
    }
    this.setState({auth: true})
  }

  click = async() => {
    if(this.state.auth === true) {
    let OS = Platform.OS
    if(OS === "ios") {
      // let dat = this.state.sDate
      // let final = dat.substring(2, 16)
      // this.setState({sendFromDate: final})
      // let dates = this.state.sDate + "," + this.state.tDate
      //   await HealthkitController.sendTime(dates)
        //this.clickHandlerAge()
        //this.clickHandlerBloodType()
        //this.clickHandlerBloodGlucose()
        this.clickHandlerHearRate()
        // this.clickHandlerBMI()
        // this.clickHandlerStep()
        //.clickHandlerBp()
        //this.clickHandlerBodyTemperature()
        this.setState({full: true})
    } else {
      let dat = this.state.sDate
      let final = dat.substring(2, 16)
      this.setState({sendFromDate: final})
      let dates = this.state.sDate + "," + this.state.tDate
      NativeModules.MyHealth.sendTime(dates)
      NativeModules.MyHealth.getHealthData((err ,result) => {
        let res = JSON.parse(result)
        this.setState({hd: {...res}, full: true})
        console.log("health!!!", this.state.hd)
        this.clickHandlerBp()
        this.clickHandlerHearRate()
    });
    }
  } else {
    Alert.alert(
      "Permission",
      "You have no Permission",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  } 
       
  }

  post = async() => {
    if(this.state.bodyTemp !== []){
      let num = jsonFile.entry.length
      num = num - 1
      for (const val of this.state.bodyTemp) {
      let value = val
      console.log(val)
      value = value.substring(0,4)
      console.log(value)
      jsonFile["entry"][num] = {"resource" : {
                                "resourceType" : "Observation",
                                "code" : {
                                    "coding" : [
                                        {
                                            "system": "http://loinc.org",
                                            "code": "18310-5",
                                            "display": "Body temperature"
                                        }
                                    ]
                                },
                                "subject": {
                                    "type" : "Patient",
                                    "reference" : "urn:uuid:patient"
                                },
                                "valueQuantity": {
                                    "value": value,
                                    "unit": "degrees C",
                                    "system": "http://unitsofmeasure.org",
                                    "code": "Cel"
                                  }
                            },
                            "request" : {
                                "method" : "POST"
                            }}
       console.log("next")
       ++num
                          }
    let json = JSON.stringify(jsonFile)
    console.log("json", json)
    Alert.alert(
      "POST",
      "Post Data to Fhir?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => postFhirData() }
      ],
      { cancelable: false }
    );
    } else {
      //
    }
  
  }

  dropList = (data) => {
    if(this.state.full === true){
    ++this.state.counter
    let cou = this.state.counter % 2
    if(cou === 1) {
    if(data === "BG"){
      this.setState({drop : true, counter: cou, type: "BG"})
    } else if(data === "BT"){
      this.setState({drop : true, counter: cou, type: "BT"})
    } else if(data === "HR"){
      this.setState({drop : true, counter: cou, type: "HR"})
    }else if(data === "BP"){
      this.setState({drop : true, counter: cou, type: "BP"})
    }else if(data === "BMI"){
      this.setState({drop : true, counter: cou, type: "BMI"})
    } else {
      console.log("none type")
    }
  } else {
    //
  }
    console.log(this.state.type)
    } else{
      console.log("empty")
    }
  }

  getDatePicker = () => {
    if(this.state.picker === false){
      this.setState({picker: true})
    } else {
      this.setState({picker: false})
    }
  }

  getDatePicker1 = () => {
    if(this.state.picker1 === false){
      this.setState({picker1: true})
    } else {
      this.setState({picker1: false})
    }
  }

  setDate = (date) => {
    let dat = date.toString()
    let final = dat.substring(0, 16)
    this.setState({date : date, sDate: final})
  }

  setDate1 = (date)=> {
    let dat = date.toString()
    let final = dat.substring(0, 16)
    this.setState({date : date, tDate: final})
  }

  getAvg = (type) => {
    let OS = Platform.OS
    if(OS === 'ios') {
    MyHealthLibrary.getAverage()
    .then(result => {
      this.setState({avg: result})
    })
    } else {
    //
    }
  }

  setPick= ()=> {
    let cou = this.state.counter % 2
    if(cou === 0){
      this.setState({drop: false, type: ""})
    } else {
      //
    }
  }

  resetAll = () => {
    this.setState({bloodGlucose: [], bmi:[], bloodPressure: [], bodyTemp:[], heartRate: [], avg : ""})
  }

  setAuthOptions = () => {
    this.setState({modal: false})
    let authObject = {
        read: [],       
        write: []
      }
      console.log("states:", this.state.hrReadCheck, this.state.hrWriteCheck, this.state.bmiReadCheck, this.state.bmiWriteCheck)
          if(this.state.hrReadCheck === true){
            authObject.read.push("HKQuantityTypeIdentifierHeartRate")
          }
          
          if (this.state.hrWriteCheck === true) {
            authObject.write.push("HKQuantityTypeIdentifierHeartRate")
          }
          
          if (this.state.bmiReadCheck === true) {
            authObject.read.push("HKQuantityTypeIdentifierBodyMassIndex")
        } 
        
        if (this.state.bmiWriteCheck === true){
            authObject.write.push("HKQuantityTypeIdentifierBodyMassIndex")
        }

       HealthkitController.requestAuthorization(authObject)

  }



    render() {
      console.log(this.state.bloodGlucose, this.state.bloodPressure, this.state.heartRate)
        return (
        <View style={styles.container}>
        <SafeAreaView>
        <ScrollView>
        <View style={{ paddingBottom: 10 }}></View>
        <TouchableOpacity style={styles.text1} onPress={() => this.setState({modal: !this.state.modal})}>
        <Text style={styles.label}>Get Authorization</Text>
        </TouchableOpacity>
        {
          this.state.modal === true ?
          <Modal
          animationType="slide"
          transparent={true}
          visible={true}
          onRequestClose={() => {
            this.setState({ modal: false });
          }}>
          <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View  style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Text style={styles.modalText}>HeartRate</Text>
            <CheckBox
              title='Read'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checked={this.state.hrReadCheck}
              checkedColor='red'
              onPress={() => this.setState({hrReadCheck: !this.state.hrReadCheck})}

            />
            <CheckBox
              title='Write'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checked={this.state.hrWriteCheck}
              checkedColor='red'
              onPress={() => this.setState({hrWriteCheck: !this.state.hrWriteCheck})}
            />
            </View>
            <View  style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Text style={styles.modalText}>BodyMassIndex</Text>
            <CheckBox
              title='Read'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checked={this.state.bmiReadCheck}
              checkedColor='red'
              onPress={() => this.setState({bmiReadCheck: !this.state.bmiReadCheck})}

            />
            <CheckBox
              title='Write'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checked={this.state.bmiWriteCheck}
              checkedColor='red'
              onPress={() => this.setState({bmiWriteCheck: !this.state.bmiWriteCheck})}
            />
            </View>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => {this.setAuthOptions()}}
            >
              <Text style={styles.textStyle}>Done</Text>
            </Pressable>
          </View>
        </View>
          </Modal>: null
        }
        <TouchableOpacity style={styles.text1} onPress={()=>{this.click()}}>
        <Text style={styles.label}>Request Health Data</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.text1} onPress={()=>{this.post()}} >
          <Text style={styles.label}>POST to fhir</Text>
        </TouchableOpacity>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{flexDirection: "row", alignItems: "center"}}>
        <Text style={styles.label}>From: {this.state.from}</Text>
        <TextInput value={this.state.sDate} 
           onChangeText={()=>{this.setDateText()}}
           accessible={true}
           onChange={()=>{this.setDateText()}}
           style={{width: 250, backgroundColor: "#D0D3D4", height: 39, color: "black"}} 
           />
        <Button title="Get Date" type="clear" onPress={()=>{this.getDatePicker(), this.setPick(), this.resetAll()}} />
   
        </View>
        <View>
        {
          this.state.picker === true ? 
        <View>
        <DatePicker
          date={this.state.date}
          mode="date"
          onDateChange={(date)=>{this.setDate(date)}}
          
        />
        <Button title="Done" onPress={()=>{this.getDatePicker()}} />
        </View>
        : null
         }
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{flexDirection: "row", alignItems: "center"}}>
        <Text style={styles.label}>To:     {this.state.from}</Text>
        <TextInput value={this.state.tDate} 
           onChangeText={()=>{this.setDateText()}}
           accessible={true}
           onChange={()=>{this.setDateText()}}
           style={{width: 250, backgroundColor: "#D0D3D4", height: 39, color: "black"}} 
           />
        <Button title="Get Date" type="clear" onPress={()=>{this.getDatePicker1(), this.setPick(), this.resetAll()}} />
   
        </View>
        <View>
        {
          this.state.picker1 === true ? 
        <View>
        <DatePicker
          date={this.state.date1}
          mode="date"
          onDateChange={(date)=>{this.setDate1(date)}}
          
        />
        <Button title="Done" onPress={()=>{this.getDatePicker1()}} />
        </View>
        : null
         }
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{ alignContent: "space-between"}}>
          <View style={{ alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>Age</Text>
        <TextInput value={this.state.age} style={{ fontSize: 18, backgroundColor:"grey", height: 40, width: 300}}textAlign='center' editable={false}/>   
        </View>
        <View style={{ paddingBottom: 20 }}></View>
        <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>Blood Type</Text>
        <TextInput value={this.state.bloodType} style={{ fontSize: 18, backgroundColor:"grey", height: 40, width: 300}} textAlign='center' editable={false}/> 
        </View> 
         </View>
         <View style={{ paddingBottom: 30 }}></View>
         <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>BloodPressure</Text>
        <View>
        <View style={{alignItems: 'stretch' , flexDirection: 'row'}}>
        <TextInput value={this.state.type === "BP"  ? this.state.bp :this.state.nil } style={{ fontSize: 18, backgroundColor:"grey", height: 40, width:300}} textAlign='center' editable={false}/>
        <Button title="V" onPress={()=>{this.dropList("BP"), this.setPick()}} />
        </View>
        <View>
          {   
            this.state.bloodPressure !== "" && this.state.drop === true && this.state.counter === 1 && this.state.type === "BP" ?
            <View>
            <ListItems dropList={this.state.bloodPressure} />    
            </View>         : null         
          }
        </View>
        </View>
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>HearRate</Text>
        <View>
        <View style={{alignItems: 'stretch' , flexDirection: 'row'}}>
        <TextInput value={this.state.type === "HR" && this.state.hr !== undefined? this.state.hr :this.state.nil } style={{ fontSize: 18, backgroundColor:"grey", height: 40, width:300}} textAlign='center'editable={false}/>
        <Button title="V" onPress={()=>{this.dropList("HR"), this.setPick()}} />
        </View>
        <View>
          {   
            this.state.heartRate !== "" && this.state.drop === true && this.state.counter === 1 && this.state.type === "HR" ?
            <View>
            <ListItems dropList={this.state.heartRate} />    
            </View>         : null         
          }
        </View>
        </View>
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>BMI</Text>
        <View>
        <View style={{alignItems: 'stretch' , flexDirection: 'row'}}>
        <TouchableOpacity>  
        <TextInput value={this.state.type === "BMI"? this.state.bmi :this.state.nil } style={{ fontSize: 18, backgroundColor:"grey", height: 40, width: 300}} textAlign='center' editable={false}/>
        </TouchableOpacity>
        <Button title="V" onPress={()=>{this.dropList("BMI"), this.setPick(), this.getBMIData()}} />
        </View>
        <View>
          {   
            this.state.bmi !== "" && this.state.drop === true && this.state.counter === 1 && this.state.type === "BMI"  ?
            <View>
            <ListItems dropList={this.state.bmi} />            
            </View> : null         
          }
        </View>
        </View>
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>BloodGlucose</Text>
        <View>
        <View style={{alignItems: 'stretch' , flexDirection: 'row'}}>
        <TextInput value={this.state.type === "BG"? this.state.bg :this.state.nil } style={{ fontSize: 18, backgroundColor:"grey", height: 40, width: 300}} textAlign='center' editable={false}/>
        <Button title="V" onPress={()=>{this.dropList("BG"), this.setPick()}} />
        </View>
        <View>
          {   
            this.state.bloodGlucose !== "" && this.state.drop === true && this.state.counter === 1 && this.state.type === "BG" ?
            <View>
            <Button title={this.state.avg === "" ? this.state.avgPlace : this.state.avg} onPress={()=>{this.getAvg()}} />
            <ListItems dropList={this.state.bloodGlucose} />    
            </View>         : null         
          }
        </View>
        </View>
        </View>
        <View style={{ paddingBottom: 30 }}></View>
        <View style={{alignContent: "space-between", alignItems: "center"}}>
        <Text style={styles.label}>BodyTemperature</Text>
        <View>
        <View style={{alignItems: 'stretch' , flexDirection: 'row'}}>
        <TextInput value={this.state.type === "BT" ? this.state.bt :this.state.nil } style={{ fontSize: 18, backgroundColor:"grey", height: 40, width: 300}} textAlign='center' editable={false}  />
        <Button title="V" onPress={()=>{this.dropList("BT"), this.setPick()}} />
        </View>
        <View>
          {   
            this.state.bodyTemp !== "" && this.state.drop === true && this.state.counter === 1 && this.state.type === "BT" ?
            <View>
            <ListItems dropList={this.state.bodyTemp} />           
            </View>  : null         
          }
        </View>
        </View>
        </View>
        <View style={{ paddingBottom: 70 }}></View>
        </ScrollView>
        </SafeAreaView>
      </View>
        );
      }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAEDED',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
   justifyContent: "flex-start"
  },
  touch: {
    backgroundColor: 'grey',
    padding: 15
  },
  text1:{
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderTopWidth:2,
    borderBottomWidth: 2,
    backgroundColor: "#D0D3D4",
    height: 40,
    borderColor: 'black',
    justifyContent: "center",
    alignItems: "center",
    fontSize: 16,
    paddingRight: 10,
    paddingLeft: 10,
    marginLeft:4,
    marginRight: 4
  },
  text: {
   borderRadius: 5, 
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderTopWidth:2,
    borderBottomWidth: 2,
    backgroundColor: "#D0D3D4",
    height: 40,
    borderColor: 'black',
    justifyContent: "center",
    alignItems: "center",
    fontSize: 16
  },
  label: {
    fontFamily: 'helvetica',
    paddingRight:10,
    fontSize: 18,
    alignItems: "center",
    paddingBottom: 5,
    paddingTop: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    textAlign: "center"
  }

});
