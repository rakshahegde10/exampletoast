import { NativeModules, NativeEventEmitter } from 'react-native';

class Controller extends NativeEventEmitter {
  constructor(nativeModule) {
    super(nativeModule);

    this.requestAuthorization = nativeModule.requestAuthorization,
    this.sendTime = nativeModule.sendTime,
    this.getHealthData = nativeModule.getHealthData
  }
}

export default new Controller(NativeModules.Controller)