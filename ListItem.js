import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, SafeAreaView, ScrollView, Platform, TouchableOpacity, Input } from 'react-native';
import { ListItem } from 'react-native-elements';




export default class ListItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
      }
    

     componentDidMount() {
        const {dropList} = this.props;
       this.setState({list: [...dropList]})
    }


        render(){
            return (
    
                <View>
                {      
                this.state.list.map((l,i) => (
                  <View key={l}>
                    <ListItem   
                    containerStyle={{padding: 4, backgroundColor:'#D3D3D3'}}
                    title={l}
                    accessible={true}
                    />
                    </View>
                ))             
              }
              </View>
            )
        }

}